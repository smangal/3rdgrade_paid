package com.mathfriendzy.listener;

/**
 * Created by root on 4/4/16.
 */
public interface LoginRegisterPopUpListener {
    public void onRegister();
    public void onLogin();
    public void onNoThanks();
}
