package com.mathfriendzy.database;

/**
 * Database Constants are defined in this interface
 * @author Yashwant Singh
 *
 */
public interface DatabaseConstant 
{
	String DATABASE_NAME 				= "LeapAhead.sqlite";
	 
	 //Language Table Constants
	 String LANGUAGE_TABLE_NAME 	= "Languages";
	 String LANGUAGE_ID 			= "Language_ID";
	 String LANGUAGE_NAME 			= "Language";
	 String LANGUAGE_CODE 			= "Language_Code";
	 
	 //Transelate Table Constants
	 String TRANSELATE_TABLE_NAME 	= "Translations";
	 String TRANSELATE_LANGUAGE_ID 	= "LANGUAGE_ID";
	 String TEXT_IDENTIFIER 		= "TEXT_IDENTIFIER";
	 String TRANSELATION 			= "TRANSLATION";
	 String APPLICATION_ID 			= "APPLICATION_ID";
	 
}
