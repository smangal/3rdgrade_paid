package com.mathfriendzy.controller.ads;

public class AdsResponseFileParser {
	
	public static CustomeHouseAdsData parseCustomeHouseAdsData(String response){
		CustomeHouseAdsData customeAdsData = new CustomeHouseAdsData();		
		customeAdsData.setImageName("icon_mathfriendzy");
		customeAdsData.setLink("http://www.mathtutorplus.com/");	
		return customeAdsData;
	}
}
